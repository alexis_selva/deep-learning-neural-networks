These are the programming assignments relative to Deep Learning - Neural Networks and Deep Learning (1st part):

- Assignment 2.1: Python Basics with numpy
- Assignment 2.2: Logistic Regression with a Neural Network mindset
- Assignment 3.1: Planar data classification with a hidden layer
- Assignment 4.1: Building your Deep Neural Network: Step by Step
- Assignment 4.2: Deep Neural Network - Application

For more information, I invite you to have a look at https://www.coursera.org/learn/neural-networks-deep-learning
